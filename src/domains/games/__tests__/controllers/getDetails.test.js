const gameRepository = require('../../repository');
const { AppError } = require('../../../../utils/error');

const getVersion = require('../../../../utils/version');
const { getDetails } = require('../../controller');

jest.mock('../../../../../db/models/index.js', () => () => {});

describe('getDetails controller', () => {
  test('should return details of the game', async () => {
    const game = {
      id: 1,
      code: 'RPS1',
      title: 'Rock-Paper-Scissors Random',
      description: 'Play Rock-Paper-Scissors with Computer (Organic Random)',
      winnerPointsEarned: 5,
      thumbnail: null,
      gameUrl: 'https://google.com',
      viewCount: 1,
      playCount: 2,
      attributes: '{}',
      numberOfRounds: 1,
    };

    const expectedResponse = {
      success: true,
      message: 'Success Get Game Details',
      data: game,
      error: null,
      version: getVersion(),
    };
    const spy = jest.spyOn(gameRepository, 'getDetails');
    spy.mockReturnValue(game);

    const mockReq = { params: { id: 1 } };
    const mockRes = { json: jest.fn() };

    await getDetails(mockReq, mockRes);

    expect(mockRes.json).toBeCalledTimes(1);
    expect(mockRes.json).toBeCalledWith(expectedResponse);
    spy.mockRestore();
  });

  test('should thowing error because game not found', async () => {
    const spy = jest.spyOn(gameRepository, 'getDetails');
    spy.mockImplementation(() => {
      throw new AppError('Game Not Found', 404);
    });

    const mockReq = { params: { id: 100 } };
    const mockRes = { json: jest.fn() };

    await expect(async () => {
      await getDetails(mockReq, mockRes);
    }).rejects.toThrow(AppError);
    spy.mockRestore();
  });
});
