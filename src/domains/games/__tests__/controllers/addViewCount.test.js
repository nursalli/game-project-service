const { addViewCount } = require('../../controller');
const repository = require('../../repository');
const getVersion = require('../../../../utils/version');
const { AppError } = require('../../../../utils/error');

jest.mock('../../../../../db/models/index.js', () => () => {});

describe('games.controller.addViewCount', () => {
  it('happy flow, game found', async () => {
    const mockReq = { params: { id: 1 } };
    const mockRes = { json: jest.fn() };

    const expectedRepoReturn = {
      id: 1,
      viewCount: 1,
    };

    const expectedControllerRes = {
      success: true,
      message: 'Success Update View Count',
      data: expectedRepoReturn,
      error: null,
      version: getVersion(),
    };

    const viewCountRepository = jest.spyOn(repository, 'addCounter');
    viewCountRepository.mockReturnValueOnce(expectedRepoReturn);

    await addViewCount(mockReq, mockRes);

    expect(mockRes.json).toBeCalledTimes(1);
    expect(mockRes.json).toBeCalledWith(expectedControllerRes);

    viewCountRepository.mockRestore();
  });

  it('sad flow, game not found', async () => {
    const mockReq = { params: { id: 100 } };
    const mockRes = { json: jest.fn() };

    const viewCountRepository = jest.spyOn(repository, 'addCounter');
    viewCountRepository.mockImplementation(() => {
      throw new AppError('Game Not Found', 404);
    });

    await expect(async () => {
      await addViewCount(mockReq, mockRes);
    }).rejects.toThrow(AppError);

    viewCountRepository.mockRestore();
  });
});
