const gameRepository = require('../../repository');
const { AppError } = require('../../../../utils/error');

const getVersion = require('../../../../utils/version');
const { getLeaderboard } = require('../../controller');

jest.mock('../../../../../db/models/index.js', () => () => {});

describe('controller getLeaderboard', () => {
  test('should return leader board', async () => {
    const leaderBoard = [
      {
        id: 12,
        name: 'Admin Binar',
        email: 'admin.binar@binar.com',
        profile_pic:
          'https://firebasestorage.googleapis.com/v0/b/game-project-service.appspot.com/o/profile-picture%2F1662252534315-Pierre-Person.jpeg?alt=media&token=7bcca533-383f-46fd-8c4e-41a241b4e497',
        badge: 'Bronze',
        points: '15',
      },
    ];

    const expectedResponse = {
      success: true,
      message: 'Success Get Games Leaderboard',
      data: leaderBoard,
      error: null,
      version: getVersion(),
    };
    const spy = jest.spyOn(gameRepository, 'getLeaderboard');
    spy.mockReturnValue(leaderBoard);

    const mockReq = { params: { id: 1 } };
    const mockRes = { json: jest.fn() };

    await getLeaderboard(mockReq, mockRes);

    expect(mockRes.json).toBeCalledTimes(1);
    expect(mockRes.json).toBeCalledWith(expectedResponse);
    spy.mockRestore();
  });

  test('should thowing error because game not found', async () => {
    const spy = jest.spyOn(gameRepository, 'getLeaderboard');
    spy.mockImplementation(() => {
      throw new AppError('Game Not Found', 404);
    });

    const mockReq = { params: { id: 100 } };
    const mockRes = { json: jest.fn() };

    await expect(async () => {
      await getLeaderboard(mockReq, mockRes);
    }).rejects.toThrow(AppError);
    spy.mockRestore();
  });
});
