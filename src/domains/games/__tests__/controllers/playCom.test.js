const gameRepository = require('../../repository');
const { AppError } = require('../../../../utils/error');

const getVersion = require('../../../../utils/version');
const { playCom } = require('../../controller');

jest.mock('../../../../../db/models/index.js', () => () => {});

describe('controller playCom', () => {
  test('should return finalize game history', async () => {
    const currentPoint = 5;
    const updateHistory = {
      id: 1,
      playerId: 1,
      gameId: 1,
      status: 'WIN',
      pointsEarned: 5,
      metaData: '[{"playerChoice":"P","comChoice":"R"}]',
      playedAt: '2022-08-09T13:33:22.768Z',
      game_id: 1,
      player_id: 1,
    };
    const latestPoint = 10;

    const newBadge = {
      id: 11,
      userId: updateHistory.playerId,
      badgeId: 1,
      badgeName: 'Bronze',
      pointsBefore: currentPoint,
      pointsAfter: latestPoint,
      earnedAt: '2022-09-22T13:33:00.434Z',
    };

    const expectedResponse = {
      success: true,
      message: 'Success Finalize Game History',
      data: {
        updateHistory: updateHistory,
        newBadge: newBadge,
      },
      error: null,
      version: getVersion(),
    };

    const spyCalcTotalPointCurrent = jest.spyOn(gameRepository, 'calcTotalPoint');
    spyCalcTotalPointCurrent.mockReturnValue(currentPoint);
    const spyUpdateHistory = jest.spyOn(gameRepository, 'updateHistory');
    spyUpdateHistory.mockReturnValue(updateHistory);
    const spyCalcTotalPointLatest = jest.spyOn(gameRepository, 'calcTotalPoint');
    spyCalcTotalPointLatest.mockReturnValue(latestPoint);
    const spyCreateBadge = jest.spyOn(gameRepository, 'createBadge');
    spyCreateBadge.mockReturnValue(newBadge);

    const mockReq = {
      user: { id: 1 },
      body: {
        id: 1,
        idHistory: 1,
        status: 'WIN',
        metaData: [
          {
            playerChoice: 'P',
            comChoice: 'R',
          },
        ],
      },
    };
    const mockRes = { json: jest.fn() };

    await playCom(mockReq, mockRes);

    expect(mockRes.json).toBeCalledTimes(1);
    expect(mockRes.json).toBeCalledWith(expectedResponse);

    spyCalcTotalPointCurrent.mockRestore();
    spyUpdateHistory.mockRestore();
    spyCalcTotalPointLatest.mockRestore();
    spyCreateBadge.mockRestore();
  });

  test('should throwing error because `calcTotalPoint` error', async () => {
    const spy = jest.spyOn(gameRepository, 'calcTotalPoint');
    spy.mockImplementation(() => {
      throw new AppError('Game Not Found', 404);
    });

    const mockReq = {
      user: { id: 1 },
      body: {
        id: 1,
        idHistory: 60,
        status: 'WIN',
        metaData: [
          {
            playerChoice: 'P',
            comChoice: 'R',
          },
        ],
      },
    };
    const mockRes = { json: jest.fn() };

    await expect(async () => {
      await playCom(mockReq, mockRes);
    }).rejects.toThrow(AppError);
    spy.mockRestore();
  });

  test('should throwing error because `updateHistory` error', async () => {
    const currentPoint = 5;
    const spyCalcTotalPointCurrent = jest.spyOn(gameRepository, 'calcTotalPoint');
    spyCalcTotalPointCurrent.mockReturnValue(currentPoint);
    const spyUpdateHistory = jest.spyOn(gameRepository, 'updateHistory');
    spyUpdateHistory.mockImplementation(() => {
      throw new AppError('Cannot Update Written Game History', 404);
    });

    const mockReq = {
      user: { id: 1 },
      body: {
        id: 1,
        idHistory: 60,
        status: 'WIN',
        metaData: [
          {
            playerChoice: 'P',
            comChoice: 'R',
          },
        ],
      },
    };
    const mockRes = { json: jest.fn() };

    await expect(async () => {
      await playCom(mockReq, mockRes);
    }).rejects.toThrow(AppError);

    spyCalcTotalPointCurrent.mockRestore();
    spyUpdateHistory.mockRestore();
  });
});
