const gameRepository = require('../../repository');
const { AppError } = require('../../../../utils/error');

const getVersion = require('../../../../utils/version');
const { getRequiredRound } = require('../../controller');

jest.mock('../../../../../db/models/index.js', () => () => {});

describe('controller getRequiredRound', () => {
  test('should return required round', async () => {
    const requiredRound = {
      gameId: 1,
      requiredRound: 3,
    };

    const expectedResponse = {
      success: true,
      message: 'Success Get Required Round',
      data: requiredRound,
      error: null,
      version: getVersion(),
    };
    const spy = jest.spyOn(gameRepository, 'getRequiredRound');
    spy.mockReturnValue(requiredRound);

    const mockReq = { params: { id: 1 } };
    const mockRes = { json: jest.fn() };

    await getRequiredRound(mockReq, mockRes);

    expect(mockRes.json).toBeCalledTimes(1);
    expect(mockRes.json).toBeCalledWith(expectedResponse);
    spy.mockRestore();
  });

  test('should thowing error because game not found', async () => {
    const spy = jest.spyOn(gameRepository, 'getRequiredRound');
    spy.mockImplementation(() => {
      throw new AppError('Game Not Found', 404);
    });

    const mockReq = { params: { id: 100 } };
    const mockRes = { json: jest.fn() };

    await expect(async () => {
      await getRequiredRound(mockReq, mockRes);
    }).rejects.toThrow(AppError);
    spy.mockRestore();
  });
});
