const { listing } = require('../../controller');
const repository = require('../../repository');
const getVersion = require('../../../../utils/version');

jest.mock('../../../../../db/models/index.js', () => () => {});

describe('games.controller.listing', () => {
  it('happy flow', async () => {
    const mockReq = { body: {} };
    const mockRes = { json: jest.fn() };

    const expectedRepoReturn = [
      {
        id: 1,
        code: 'RPS1',
        title: 'Rock-Paper-Scissors Random',
        description: 'Play Rock-Paper-Scissors with Computer (Organic Random)',
        winnerPointsEarned: 5,
        thumbnail: null,
        gameUrl: 'https://google.com',
        viewCount: 0,
        playCount: 0,
        attributes: '{}',
        numberOfRounds: 1,
      },
    ];

    const expectedControllerRes = {
      success: true,
      message: 'Success List Games',
      data: expectedRepoReturn,
      error: null,
      version: getVersion(),
    };

    const listRepository = jest.spyOn(repository, 'list');
    listRepository.mockReturnValueOnce(expectedRepoReturn);

    await listing(mockReq, mockRes);

    expect(mockRes.json).toBeCalledTimes(1);
    expect(mockRes.json).toBeCalledWith(expectedControllerRes);

    listRepository.mockRestore();
  });
});
