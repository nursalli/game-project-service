const getVersion = require('../../../../utils/version');
const { getIsPlayed } = require('../../controller');
const gameRepository = require('../../repository');

jest.mock('../../../../../db/models/index.js', () => () => {});

describe('getIsPLayed controller', () => {
  test('should return true', async () => {
    const expectedResponse = {
      success: true,
      message: 'Success Game is Played by User',
      data: { isPlayed: true },
      error: null,
      version: getVersion(),
    };

    const mockReq = { params: { id: 1 }, user: { id: 1 } };
    const mockRes = { json: jest.fn() };

    const spy = jest.spyOn(gameRepository, 'getIsPlayed');
    spy.mockReturnValue(true);

    await getIsPlayed(mockReq, mockRes);
    expect(mockRes.json).toBeCalledTimes(1);
    expect(mockRes.json).toBeCalledWith(expectedResponse);

    spy.mockRestore();
  });

  test('should return false', async () => {
    const expectedResponse = {
      success: true,
      message: 'Success Game is Played by User',
      data: { isPlayed: false },
      error: null,
      version: getVersion(),
    };

    const mockReq = { params: { id: 1 }, user: { id: 1 } };
    const mockRes = { json: jest.fn() };

    const spy = jest.spyOn(gameRepository, 'getIsPlayed');
    spy.mockReturnValue(false);

    await getIsPlayed(mockReq, mockRes);
    expect(mockRes.json).toBeCalledTimes(1);
    expect(mockRes.json).toBeCalledWith(expectedResponse);

    spy.mockRestore();
  });
});
