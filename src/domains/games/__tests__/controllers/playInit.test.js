const gameRepository = require('../../repository');
const { AppError } = require('../../../../utils/error');

const getVersion = require('../../../../utils/version');
const { playInit } = require('../../controller');

jest.mock('../../../../../db/models/index.js', () => () => {});

describe('controller playInit', () => {
  test('should return initiating new game', async () => {
    const init = {
      id: 1,
    };

    const expectedResponse = {
      success: true,
      message: 'Success Initiate Game History',
      data: init,
      error: null,
      version: getVersion(),
    };
    const spy = jest.spyOn(gameRepository, 'playInit');
    spy.mockReturnValue(init);

    const mockReq = {
      user: { id: 1 },
      body: {
        id: 1,
        playedAt: '2022-08-09T13:33:22.768Z',
      },
    };
    const mockRes = { json: jest.fn() };

    await playInit(mockReq, mockRes);

    expect(mockRes.json).toBeCalledTimes(1);
    expect(mockRes.json).toBeCalledWith(expectedResponse);

    spy.mockRestore();
  });

  test('should thowing error because game not found', async () => {
    const spy = jest.spyOn(gameRepository, 'playInit');
    spy.mockImplementation(() => {
      throw new AppError('Game Not Found', 404);
    });

    const mockReq = {
      user: { id: 1 },
      body: {
        id: 100,
        playedAt: '2022-08-09T13:33:22.768Z',
      },
    };
    const mockRes = { json: jest.fn() };

    await expect(async () => {
      await playInit(mockReq, mockRes);
    }).rejects.toThrow(AppError);
    spy.mockRestore();
  });
});
