const { addPlayCount } = require('../../controller');
const repository = require('../../repository');
const getVersion = require('../../../../utils/version');
const { AppError } = require('../../../../utils/error');

jest.mock('../../../../../db/models/index.js', () => () => {});

describe('games.controller.addPlayCount', () => {
  it('happy flow, game found', async () => {
    const mockReq = { params: { id: 1 } };
    const mockRes = { json: jest.fn() };

    const expectedRepoReturn = {
      id: 1,
      playCount: 1,
    };

    const expectedControllerRes = {
      success: true,
      message: 'Success Update Play Count',
      data: expectedRepoReturn,
      error: null,
      version: getVersion(),
    };

    const playCountRepository = jest.spyOn(repository, 'addCounter');
    playCountRepository.mockReturnValueOnce(expectedRepoReturn);

    await addPlayCount(mockReq, mockRes);

    expect(mockRes.json).toBeCalledTimes(1);
    expect(mockRes.json).toBeCalledWith(expectedControllerRes);

    playCountRepository.mockRestore();
  });

  it('sad flow, game not found', async () => {
    const mockReq = { params: { id: 100 } };
    const mockRes = { json: jest.fn() };

    const playCountRepository = jest.spyOn(repository, 'addCounter');
    playCountRepository.mockImplementation(() => {
      throw new AppError('Game Not Found', 404);
    });

    await expect(async () => {
      await addPlayCount(mockReq, mockRes);
    }).rejects.toThrow(AppError);

    playCountRepository.mockRestore();
  });
});
