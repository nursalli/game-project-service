const { updateProfilePic } = require('../../controller');
const repository = require('../../repository');
const getVersion = require('../../../../utils/version');

jest.mock('../../../../../db/models/index.js', () => () => {});

describe('users.controller.updateProfilePic', () => {
  it('happy flow, success update profile picture', async () => {
    const mockReq = {
      body: {
        profilePic: 'https://firebase-store/image.jpg',
      },
      user: { id: 1 },
    };
    const mockRes = { json: jest.fn() };

    const expectedRepoReturn = {
      profilePic: 'https://firebase-store/image.jpg',
    };

    const expectedControllerRes = {
      success: true,
      message: 'Success update profile picture',
      data: expectedRepoReturn,
      error: null,
      version: getVersion(),
    };

    const updateProfilePicRepository = jest.spyOn(repository, 'updateProfilePic');
    updateProfilePicRepository.mockReturnValueOnce(expectedRepoReturn);

    await updateProfilePic(mockReq, mockRes);

    expect(mockRes.json).toBeCalledTimes(1);
    expect(mockRes.json).toBeCalledWith(expectedControllerRes);

    updateProfilePicRepository.mockRestore();
  });
});
