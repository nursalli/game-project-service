const { getMyBio } = require('../../controller');
const repository = require('../../repository');
const getVersion = require('../../../../utils/version');
const { AppError } = require('../../../../utils/error');

jest.mock('../../../../../db/models/index.js', () => () => {});

describe('users.controller.getMyBio', () => {
  it('happy flow, success get user\'s biodata', async () => {
    const mockReq = { params: { userId: 12 } };
    const mockRes = { json: jest.fn() };

    const expectedRepoReturn = {
      id: 12,
      email: 'ferry@mail.com',
      firstName: 'G.',
      lastName: 'Ferry',
      profilePic:
        'https://firebasestorage.googleapis.com/v0/b/game-project-service.appspot.com/o/profile-picture%2F1662285350771-acd92795ee96376bf9db1cd691ec9f83.jpg?alt=media&token=6ea53262-ea77-45b4-a79d-83ec06c4947a',
      bio: 'Ini Ferry yang edit kedua kali',
      birthday: '2020-03-01',
      country: 'USA',
    };

    const expectedControllerRes = {
      success: true,
      message: 'Success Get user\'s biodata',
      data: expectedRepoReturn,
      error: null,
      version: getVersion(),
    };

    const getMyBioRepository = jest.spyOn(repository, 'getMyBio');
    getMyBioRepository.mockReturnValueOnce(expectedRepoReturn);

    await getMyBio(mockReq, mockRes);

    expect(mockRes.json).toBeCalledTimes(1);
    expect(mockRes.json).toBeCalledWith(expectedControllerRes);

    getMyBioRepository.mockRestore();
  });

  it('sad flow, user not found', async () => {
    const mockReq = { params: { userId: 100 } };
    const mockRes = { json: jest.fn() };

    const getMyBioRepository = jest.spyOn(repository, 'getMyBio');
    getMyBioRepository.mockImplementation(() => {
      throw new AppError('User Not Found', 404);
    });

    await expect(async () => {
      await getMyBio(mockReq, mockRes);
    }).rejects.toThrow(AppError);

    getMyBioRepository.mockRestore();
  });
});
